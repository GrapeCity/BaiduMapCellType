#### 插件打包下载
- 活字格图片轮转类型插件下载：https://gcdn.grapecity.com.cn/forum.php?mod=viewthread&tid=48662
- 活字格官网：https://www.grapecity.com.cn/solutions/huozige

#### BaiduMapCellType
功能：使用百度地图进行定位。

#### 操作步骤
1. 在“版本更新记录”表中，下载对应版本的插件。
2. 安装插件并重启活字格设计器后，打开单元格类型，会出现刚安装好的地图插件。
3. 选择一片单元格区域并设置其单元格类型为地图类型。

![图1  设置单元格类型](https://gcdn.grapecity.com.cn/data/attachment/forum/201807/18/120023fyi3669jl9686209.png)

4. 在属性设置区的“单元格设置”页签，对签名进行设置，设置说明如下
-  **经度：** 关闭定位时地图中心点的经度。
-  **纬度：** 关闭定位时地图中心点的纬度。
-  **级别：** 地图的默认缩放级别。
-  **定位：** 是否开启定位。
-  **测距：** 是否开启测距功能。右键可设置起点坐标，左键进行坐标测量，双击设置结束坐标。
-  **只读：** 只读时不能设置新位置。
5. 运行页面，如下图所示

![图2  地图界面](https://gcdn.grapecity.com.cn/data/attachment/forum/201807/18/120139a0vfysuhrfwe3ve3.jpg)